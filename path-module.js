const path =require('path');
console.log(path.sep)

const filePath= path.join('/assests','404.html')
console.log(filePath)

const baseName = path.basename(filePath)
console.log(baseName)

const absolutePath = path.resolve(__dirname,'assests','404.html')
console.log(absolutePath)