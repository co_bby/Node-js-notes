const express = require('express');
// Express App
 const app = express()
// register view engine
app.set('view engine', 'ejs');


//  listen to requests
app.listen(3000);
//send file()
app.use((req, res, next)=>{
	console.log("making request");
	console.log(req.method);
	console.log(req.path);
	console.log(req.hostname);
	next();
});
app.get('/', ( req,res) => {
	const blogs =[
		{title:"Breaking bad", snippet:'Cobby is a nice guy'},
		{title:"Breaking bad", snippet:'Cobby is a nice guy'},
		{title:"Breaking bad", snippet:'Cobby is a nice guy'}
	]
	res.render('index', {title:"home", blogs:blogs} );
})
app.get('/about', (req, res)=>{
	res.render('about');
});

// app.get('/about',(req, res)=>{
// 	res.sendFile('./assests/about.html',{root:__dirname})
// })
// // Redirects
// app.get('/about-us', (req, res)=>{
// 	res.redirect('/about')
// })
// // Error Page
app.use((req, res)=>{
	res.status(404).render('404')
})