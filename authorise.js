const authorize = (req, res, next) => {
  // this is for tutorial purposes only;
  // this is not how to authorize usee authentication;
  const { user } = req.query;
  if (user === "cobby") {
    req.user = { name: "cobby", id: 3 };
  } else {
    res.status(401).send("unauthorized");
  }
  next();
};

module.exports = authorize;
