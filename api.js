const express = require("express");

const app = express();
const { products } = require("./data");

app.get("/", (req, res) => {
  res.send('<h1>Home Page</h1><a href="/api/products">Products</a>');
});
app.get("/api/products", (req, res) => {
  const newProducts = products.map((product) => {
    const { id, name, image } = product;
    return { id, name, image };
  });
  res.json(newProducts);
});

// Request Parameter
app.get("/api/products/:productID", (req, res) => {
  // console.log(req);
  // console.log(req.param);
  const { productID } = req.params;

  const singleProduct = products.find(
    (product) => (product.id = Number(productID))
  );
  if (!singleProduct) {
    res.status(404).send("product does not exist");
  }
  return res.json(singleProduct);
});

// Request Query

app.get("/api/v1/query", (req, res) => {
  // console.log(req.query);
  const { search, limit } = req.query;
  let sortedProducts = [...products];
  if (search) {
    sortedProducts = sortedProducts.filter((product) => {
      return product.name.startsWith(search);
    });
  }
  if (limit) {
    sortedProducts = sortProducts.slice(0, Number(limit));
  }

  res.status(200).json(sortedProducts);
});
if (sortProducts.length > 1) {
  res.status(200).send("no product matched your search");
}
app.listen(5000, () => {
  console.log("listening on port 5000...");
});
