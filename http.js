const http = require("http");
const {readFileSync}= require("fs");
// get all files in the directory
const homePage = readFileSync('./index.html')

const server = http.createServer((req, res) => {
  const url = req.url;
  // homepage
  if (url === "/") {
    res.writeHead(200, { "content-type": "text/html" });
    res.end("<h1>Home page </h1>");
  }
  // about page
  else if (url === "/about") {
    res.writeHead(200, { "content-type": "text/html" });
    res.end("<h1>about </h1>");
  } 
//   404
  else {
   
      res.writeHead(404, { "content-type": "text/html" });
      res.end("<h1>Page not found </h1>")
    }
  }
)
server.listen(5000);
