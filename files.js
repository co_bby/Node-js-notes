const fs = require('fs');
//reading a file
//it is asynchronous
//  fs.readFile ('./docs.txt',  (err, data)=>{
// 	if (err){ 
// 		console.log (err);
// 	} 
 	 
// 	console.log (data.toString());

//  }) 
//  console.log("last line") 


// writing file
//when the file does not exist it will create another file
// fs.writeFile ('./docs.txt','i am coming', () => {
// 	console.log('file written');
// });

// Directory
// if (!fs.existsSync('./assests')){
// fs.mkdir('./assests', (err) =>{
// 	if (err){
// 		console.log(err);
// 	}
// 	console.log('Directory created');
// });
// }
// else{
// fs.rmdir('./assests', (err)=>{
// 	if(err){
// 		console.log(err)
// 	}
// 	console.log('folder deleted');
// })
// }
// //deleting a files
// if(fs.existsSync('./docs.txt')){
// 	fs.unlink('./assests', (err)=>{
// 	if (err){
// 		console.log(err)
// 	}
// 	console.log('file deleted')	
// 	})
// }

const getText= (path)=>{
	return new Promise((resolve, reject)=>{
fs.readFile (path,'utf8',  (err, data)=>{
	if (err){
		reject(err);
	}
	else{
		resolve(data);
	}
})
	})
}


const start = async(()=>{
	const first = await getText('./docs.txt');
	console.log(first)
})

start()
// getText('./docs.txt')
// .then(result=> console.log(result))
// .catch((err)=> console.log(err));