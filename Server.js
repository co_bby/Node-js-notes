// import http module
const http = require('http')
const fs = require('fs')
const _ = require('lodash')
// create http server
const server = http.createServer((req, res)=>{
	
// lodash
const num = _.random(2, 10)
console.log(num)
	// set header content type
	res.setHeader('content-type', 'text/html')
	let path = './assests/';

	switch(req.url){
		case '/':
			path += 'index.html';
			res.statusCode = 200;
			break;
		case '/about':
		 	path += 'about.html';
			 res.statusCode = 200;
			 break;
		case '/about-us':
			res.statusCode = 301;
			res.header=('Location','/about')
			res.end();
			break;
		default:
			path += '404.html'
			res.statusCode = 404
			break;
	}
	
	// sending an html files
	fs.readFile(path, (err, data)=>{
		if (err){
		console.log(err)
		res.end();
		}
		else{
			res.write(data)
			res.end()
		}
	})

});
server.listen(3000, 'localhost', ()=>{
	console.log("server listening on port 3000")
})

